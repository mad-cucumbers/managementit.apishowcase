﻿using InventoryIT.WebHost.Contracts.Requests.SpecificationRequests;
using InventoryIT.WebHost.Contracts.Responses.SpecificationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class SpecificationController : BaseController<SpecificationViewModel,
                                                SpecificationViewModel,
                                                GetByIdSpecificationRequest,
                                                CreateSpecificationRequest,
                                                UpdateSpecificationRequest,
                                                DeleteSpecificationRequest,
                                                GetAllSpecificationResponse,
                                                GetByIdSpecificationResponse>
    {
        public SpecificationController(
            IRequestClient<SpecificationViewModel> getAll,
            IRequestClient<GetByIdSpecificationRequest> getById,
            IRequestClient<CreateSpecificationRequest> create,
            IRequestClient<UpdateSpecificationRequest> update,
            IRequestClient<DeleteSpecificationRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "Specification";
        }
    }
}
