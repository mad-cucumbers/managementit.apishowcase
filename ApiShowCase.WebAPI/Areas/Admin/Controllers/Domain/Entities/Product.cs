﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class Product : BaseEntity
    {
        public string ProductId { get; set; }
        public virtual ProductSpecificationBlank ProductSpecificationBlank { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string CategoryName { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public string Vendor { get; set; }
        public virtual List<InventoryItem> InventoryItems { get; set; }
        public virtual List<OrderRecord> OrderRecords { get; set; }
    }
}
