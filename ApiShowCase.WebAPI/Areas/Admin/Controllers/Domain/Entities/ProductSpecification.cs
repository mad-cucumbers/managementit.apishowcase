﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class ProductSpecification : BaseEntity
    {
        public string ProductSpecificationId { get; set; }
        public string ProductSpecificationBlankId { get; set; }
        public virtual ProductSpecificationBlank ProductSpecificationBlank { get; set; }
        public string ProductSpecificationValue { get; set; }
    }
}
