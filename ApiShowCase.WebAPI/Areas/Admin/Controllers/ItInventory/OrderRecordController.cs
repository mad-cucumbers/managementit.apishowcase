﻿using InventoryIT.WebHost.Contracts.Requests.OrderRecordRequests;
using InventoryIT.WebHost.Contracts.Responses.OrderRecordResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class OrderRecordController : BaseController<OrderRecordViewModel,
                                                OrderRecordViewModel,
                                                GetByIdOrderRecordRequest,
                                                CreateOrderRecordRequest,
                                                UpdateOrderRecordRequest,
                                                DeleteOrderRecordRequest,
                                                GetAllOrderRecordResponse,
                                                GetByIdOrderRecordResponse>
    {
        public OrderRecordController(
            IRequestClient<OrderRecordViewModel> getAll,
            IRequestClient<GetByIdOrderRecordRequest> getById,
            IRequestClient<CreateOrderRecordRequest> create,
            IRequestClient<UpdateOrderRecordRequest> update,
            IRequestClient<DeleteOrderRecordRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "OrderRecord";
        }
    }
}
