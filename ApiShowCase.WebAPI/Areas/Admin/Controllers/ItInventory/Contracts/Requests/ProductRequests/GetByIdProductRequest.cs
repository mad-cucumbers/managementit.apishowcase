﻿using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Requests.ProductRequests
{
    public class GetByIdProductRequest : GetByIdEntityRequest<int>
    {
    }
}
