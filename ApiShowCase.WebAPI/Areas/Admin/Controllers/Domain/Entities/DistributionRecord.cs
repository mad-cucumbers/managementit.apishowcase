﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class DistributionRecord : BaseEntity
    {
        public string DistributionId { get; set; }
        public virtual Distribution Distribution { get; set; }
        public string InventoryItemId { get; set; }
        public virtual InventoryItem InventoryItem { get; set; }
        public int WorkplaceId { get; set; }
        public virtual Workplace Workplace { get; set; }
        public float Quantity { get; set; }
    }
}
