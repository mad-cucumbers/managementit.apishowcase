﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class ProductCategoryViewModel : BaseEntity
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool IsBatch { get; set; }
        public bool IsSpecRequired { get; set; }
    }
}
