﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class SpecificationViewModel : BaseEntity
    {
        public string SpecificationId { get; set; }
        public bool IsRequered { get; set; }
        public int SpecificationBlankId { get; set; }
    }
}
