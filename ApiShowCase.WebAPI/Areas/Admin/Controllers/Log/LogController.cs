﻿using Contracts.Enums;
using Contracts.Logs;
using Contracts.ResponseModels;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.Log
{
    [Area("Admin")]
    [Route("[controller]")]
    public class LogController : Controller
    {
        private readonly IRequestClient<LogMessage> _getAll;
        private readonly IRequestClient<DeleteLogRequest> _deleteById;
        private readonly IRequestClient<DeleteRangeLogRequest> _deleteRange;
        private readonly IRequestClient<DeleteSelectLogRequest> _deleteSelected;

        public LogController(IRequestClient<DeleteRangeLogRequest> deleteRange,
            IRequestClient<LogMessage> all,
            IRequestClient<DeleteLogRequest> deleteById, 
            IRequestClient<DeleteSelectLogRequest> deleteSelected)
        {
            _deleteRange = deleteRange ?? throw new ArgumentNullException(nameof(deleteRange));
            _getAll = all ?? throw new ArgumentNullException(nameof(all));
            _deleteById = deleteById ?? throw new ArgumentNullException(nameof(deleteById));
            _deleteSelected = deleteSelected ?? throw new ArgumentNullException(nameof(deleteSelected));
        }

        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<NotificationViewModel<IEnumerable<LogMessage>>>> List()
        {
            try
            {
                var result = await _getAll.GetResponse<AllLogMessageResponse>(new LogMessage());
                if (result.Message.Notification.Type != NotificationType.Success)
                    return Ok(result.Message.Notification);

                return Ok(new NotificationViewModel<IEnumerable<LogMessage>>(result.Message.Model));
            }
            catch (Exception)
            {
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<NotificationViewModel>> Delete(string id)
        {
            if (string.IsNullOrEmpty(id)) return Ok(new NotificationViewModel(new[] { TypeOfErrors.BadRequest }));

            try
            {
                var result = await _deleteById.GetResponse<NotificationViewModel>(new DeleteLogRequest { LogId = id });
                return Ok(result.Message);
            }
            catch (Exception)
            {
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpDelete]
        [Route("deleteRange")]
        public async Task<ActionResult<NotificationViewModel>> DeleteRange()
        {
            try
            {
                var result = await _deleteRange.GetResponse<NotificationViewModel>(new DeleteRangeLogRequest());
                return Ok(result.Message);
            }
            catch (Exception)
            {
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpPost]
        [Route("deleteSelected")]
        public async Task<ActionResult<NotificationViewModel>> DeleteSelected(string ids)
        {
            var request = JsonSerializer.Deserialize<DeleteSelectLogRequest>(ids);
            if (request.LogIds == null) return Ok(new NotificationViewModel(new[] { TypeOfErrors.BadRequest }));
            if(!request.LogIds.Any()) return Ok(new NotificationViewModel(new[] { TypeOfErrors.BadRequest }));

            try
            {
                var result = await _deleteSelected.GetResponse<NotificationViewModel>(request);
                return Ok(result.Message);
            }
            catch (Exception)
            {
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }
    }
}
