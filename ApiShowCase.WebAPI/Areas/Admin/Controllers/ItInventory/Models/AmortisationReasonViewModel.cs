﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class AmortisationReasonViewModel : BaseEntity
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
    }
}
