﻿using InventoryIT.WebHost.Contracts.Requests.InventoryItemSpecificationRequests;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemSpecificationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class InventoryItemSpecificationController : BaseController<InventoryItemSpecificationViewModel,
                                                InventoryItemSpecificationViewModel,
                                                GetByIdInventoryItemSpecificationRequest,
                                                CreateInventoryItemSpecificationRequest,
                                                UpdateInventoryItemSpecificationRequest,
                                                DeleteInventoryItemSpecificationRequest,
                                                GetAllInventoryItemSpecificationResponse,
                                                GetByIdInventoryItemSpecificationResponse>
    {
        public InventoryItemSpecificationController(
            IRequestClient<InventoryItemSpecificationViewModel> getAll,
            IRequestClient<GetByIdInventoryItemSpecificationRequest> getById,
            IRequestClient<CreateInventoryItemSpecificationRequest> create,
            IRequestClient<UpdateInventoryItemSpecificationRequest> update,
            IRequestClient<DeleteInventoryItemSpecificationRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "InventoryItemSpecification";
        }
    }
}
