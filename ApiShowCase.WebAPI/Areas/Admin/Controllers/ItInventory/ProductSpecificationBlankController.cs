﻿using InventoryIT.WebHost.Contracts.Requests.ProductSpecificationBlankRequests;
using InventoryIT.WebHost.Contracts.Responses.ProductSpecificationBlankResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class ProductSpecificationBlankController : BaseController<ProductSpecificationBlankViewModel,
                                                ProductSpecificationBlankViewModel,
                                                GetByIdProductSpecificationBlankRequest,
                                                CreateProductSpecificationBlankRequest,
                                                UpdateProductSpecificationBlankRequest,
                                                DeleteProductSpecificationBlankRequest,
                                                GetAllProductSpecificationBlankResponse,
                                                GetByIdProductSpecificationBlankResponse>
    {
        public ProductSpecificationBlankController(
            IRequestClient<ProductSpecificationBlankViewModel> getAll,
            IRequestClient<GetByIdProductSpecificationBlankRequest> getById,
            IRequestClient<CreateProductSpecificationBlankRequest> create,
            IRequestClient<UpdateProductSpecificationBlankRequest> update,
            IRequestClient<DeleteProductSpecificationBlankRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "ProductSpecificationBlank";
        }
    }
}
