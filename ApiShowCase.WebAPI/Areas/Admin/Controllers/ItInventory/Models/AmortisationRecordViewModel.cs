﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class AmortisationRecordViewModel : BaseEntity
    {
        public string AmortisationId { get; set; }
        public virtual InventoryItemViewModel InventoryItem { get; set; }
        public float Quantity { get; set; }
        public AmortisationReasonViewModel AmortisationReason { get; set; }
    }
}
