﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class Amortisation : BaseEntity
    {
        public string AmortisationId { get; set; }
        public int InventoryItemTransactionId { get; set; }
        public virtual InventoryItemTransaction InventoryItemTransaction { get; set; }        
        public DateTime AmortisationDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public virtual List<AmortisationRecord> AmortisationRecords { get; set; }
    }
}