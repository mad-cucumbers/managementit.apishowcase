﻿using InventoryIT.Core.Domain;
using System;
using System.Collections.Generic;

namespace InventoryIT.WebHost.Models
{
    public class DistributionViewModel : BaseEntity
    {
        public string DistributionId { get; set; }
        public DateTime DistributionDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public int ResponsibleStockEmployeeId { get; set; }
        public List<DistributionRecordViewModel> DistributionRecords { get; set; }
    }
}
