﻿using InventoryIT.WebHost.Contracts.Requests.ProductRequests;
using InventoryIT.WebHost.Contracts.Responses.ProductResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class ProductController : BaseController<ProductViewModel,
                                                ProductViewModel,
                                                GetByIdProductRequest,
                                                CreateProductRequest,
                                                UpdateProductRequest,
                                                DeleteProductRequest,
                                                GetAllProductResponse,
                                                GetByIdProductResponse>
    {
        public ProductController(
            IRequestClient<ProductViewModel> getAll,
            IRequestClient<GetByIdProductRequest> getById,
            IRequestClient<CreateProductRequest> create,
            IRequestClient<UpdateProductRequest> update,
            IRequestClient<DeleteProductRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "Product";
        }
    }
}
