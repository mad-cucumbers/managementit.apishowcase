﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Requests.BaseRequests
{
    public class BaseRequest<TEntityRequest>
    {
        public TEntityRequest ModelRequest { get; set; }
    }
}
