﻿using InventoryIT.WebHost.Contracts.Responses.BaseResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Responses.OrderResponses
{
    public class GetAllOrderResponse : GetAllEntityResponse<OrderViewModel>
    {
    }
}
