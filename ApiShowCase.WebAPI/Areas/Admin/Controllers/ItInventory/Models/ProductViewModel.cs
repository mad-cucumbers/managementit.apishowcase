﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class ProductViewModel : BaseEntity
    {
        public string ProductId { get; set; }
        public ProductSpecificationBlankViewModel ProductSpecificationBlank { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string CategoryName { get; set; }
        public ProductCategoryViewModel ProductCategory { get; set; }
        public string Vendor { get; set; }
    }
}
