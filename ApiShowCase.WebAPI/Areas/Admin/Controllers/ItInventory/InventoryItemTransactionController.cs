﻿using InventoryIT.WebHost.Contracts.Requests.InventoryItemTransactionRequests;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemTransactionResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class InventoryItemTransactionController : BaseController<InventoryItemTransactionViewModel,
                                                InventoryItemTransactionViewModel,
                                                GetByIdInventoryItemTransactionRequest,
                                                CreateInventoryItemTransactionRequest,
                                                UpdateInventoryItemTransactionRequest,
                                                DeleteInventoryItemTransactionRequest,
                                                GetAllInventoryItemTransactionResponse,
                                                GetByIdInventoryItemTransactionResponse>
    {
        public InventoryItemTransactionController(
            IRequestClient<InventoryItemTransactionViewModel> getAll,
            IRequestClient<GetByIdInventoryItemTransactionRequest> getById,
            IRequestClient<CreateInventoryItemTransactionRequest> create,
            IRequestClient<UpdateInventoryItemTransactionRequest> update,
            IRequestClient<DeleteInventoryItemTransactionRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "InventoryItemTransaction";
        }
    }
}
