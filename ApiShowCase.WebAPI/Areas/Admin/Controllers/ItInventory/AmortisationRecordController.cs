﻿using InventoryIT.WebHost.Contracts.Requests.AmortisationRecordRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationRecordResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class AmortisationRecordController : BaseController<AmortisationRecordViewModel,
                                                AmortisationRecordViewModel,
                                                GetByIdAmortisationRecordRequest,
                                                CreateAmortisationRecordRequest,
                                                UpdateAmortisationRecordRequest,
                                                DeleteAmortisationRecordRequest,
                                                GetAllAmortisationRecordResponse,
                                                GetByIdAmortisationRecordResponse>
    {
        public AmortisationRecordController(
            IRequestClient<AmortisationRecordViewModel> getAll,
            IRequestClient<GetByIdAmortisationRecordRequest> getById,
            IRequestClient<CreateAmortisationRecordRequest> create,
            IRequestClient<UpdateAmortisationRecordRequest> update,
            IRequestClient<DeleteAmortisationRecordRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "AmortisationRecord";
        }
    }
}
