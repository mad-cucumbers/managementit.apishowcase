﻿using InventoryIT.WebHost.Contracts.Requests.ProductCategoryRequests;
using InventoryIT.WebHost.Contracts.Responses.ProductCategoryResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class ProductCategoryController : BaseController<ProductCategoryViewModel,
                                                ProductCategoryViewModel,
                                                GetByIdProductCategoryRequest,
                                                CreateProductCategoryRequest,
                                                UpdateProductCategoryRequest,
                                                DeleteProductCategoryRequest,
                                                GetAllProductCategoryResponse,
                                                GetByIdProductCategoryResponse>
    {
        public ProductCategoryController(
            IRequestClient<ProductCategoryViewModel> getAll,
            IRequestClient<GetByIdProductCategoryRequest> getById,
            IRequestClient<CreateProductCategoryRequest> create,
            IRequestClient<UpdateProductCategoryRequest> update,
            IRequestClient<DeleteProductCategoryRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "ProductCategory";
        }
    }
}
