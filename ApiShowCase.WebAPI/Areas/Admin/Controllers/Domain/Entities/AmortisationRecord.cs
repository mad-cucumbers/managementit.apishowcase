﻿using System.ComponentModel.DataAnnotations;

namespace InventoryIT.Core.Domain.Entities
{
    public class AmortisationRecord : BaseEntity
    {
        public string AmortisationId { get; set; }
        public virtual Amortisation Amortisation { get; set; }
        public string InventoryItemId { get; set; }
        public virtual InventoryItem InventoryItem { get; set; }
        public float Quantity { get; set; }
        public string AmortisationReasonId { get; set; }
        public virtual AmortisationReason AmortisationReason { get; set; }
    }
}
