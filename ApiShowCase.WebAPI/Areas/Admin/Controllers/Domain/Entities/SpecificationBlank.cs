﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class SpecificationBlank : BaseEntity
    {
        public string SpecificationBlankId { get; set; }
        public virtual List<Specification> Specifications { get; set; }
        public virtual List<ProductSpecificationBlank> ProductSpecificationBlanks { get; set;  }
    }
}
