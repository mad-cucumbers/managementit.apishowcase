﻿using Contracts.Enums;
using Contracts.Logs;
using Contracts.ResponseModels;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Contracts.Responses.BaseResponses;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    public class BaseController<TViewModel,
                                TGetAllRequest,
                                TGetByIdRequest,
                                TCreateRequest,
                                TUpdateRequest,
                                TDeleteRequest,
                                TGetAllEntityResponse,
                                TGetByIdEntityResponse> 
                                : ControllerBase
                                where TViewModel : InventoryIT.Core.Domain.BaseEntity, new()
                                where TGetAllRequest : class, new()
                                where TGetByIdRequest : GetByIdEntityRequest<int>, new()
                                where TCreateRequest : CreateEntityRequest<TViewModel>, new()
                                where TUpdateRequest : UpdateEntityRequest<TViewModel>, new()
                                where TDeleteRequest : DeleteEntityRequest<int>, new()
                                where TGetAllEntityResponse : GetAllEntityResponse<TViewModel>
                                where TGetByIdEntityResponse : GetByIdEntityResponse<TViewModel>
    {
        private readonly IRequestClient<TGetAllRequest> _getAll;
        private readonly IRequestClient<TGetByIdRequest> _getById;
        private readonly IRequestClient<TCreateRequest> _create;
        private readonly IRequestClient<TUpdateRequest> _update;
        private readonly IRequestClient<TDeleteRequest> _delete;
        private readonly IPublishEndpoint _publishEndpoint;
        protected string addressLog;
        public BaseController(IRequestClient<TGetAllRequest> getAll,
            IRequestClient<TGetByIdRequest> getById,
            IRequestClient<TCreateRequest> create,
            IRequestClient<TUpdateRequest> update,
            IRequestClient<TDeleteRequest> delete,
            IPublishEndpoint publishEndpoint)
        {
            _getAll = getAll ?? throw new ArgumentNullException(nameof(getAll));
            _create = create ?? throw new ArgumentNullException(nameof(create));
            _update = update ?? throw new ArgumentNullException(nameof(update));
            _delete = delete ?? throw new ArgumentNullException(nameof(delete));
            _getById = getById ?? throw new ArgumentNullException(nameof(getById));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        }

        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<NotificationViewModel<IEnumerable<TViewModel>>>> List()
        {
            try
            {
                var result = await _getAll.GetResponse<TGetAllEntityResponse>(new TGetAllRequest());
                if (result.Message.Notification.Type != NotificationType.Success)
                {
                    var message = new CreateLog($"{addressLog}/list", result.Message.Notification.AspNetException, NotificationType.Error, HttpContext.User?.Identity?.Name);
                    await _publishEndpoint.Publish(message);
                    return Ok(result.Message.Notification);
                }
                var response = new NotificationViewModel<IEnumerable<TViewModel>>(result.Message.Model);
                return Ok(response);
            }
            catch (Exception e)
            {
                var message = new CreateLog($"{addressLog}/list", e.Message, NotificationType.Error, HttpContext.User?.Identity?.Name);
                await _publishEndpoint.Publish(message);
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.OrganizationEntityError }));
            }
        }

        [HttpGet]
        [Route("details")]
        public async Task<ActionResult<NotificationViewModel<TViewModel>>> Details(int id)
        {
            if (id == 0)
            {
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.BadRequest }));
            }
            try
            {
                var result = await _getById.GetResponse<TGetByIdEntityResponse>(new TGetByIdRequest { ModelRequest = id });
                if (result.Message.Notification.Type != NotificationType.Success)
                {
                    var message = new CreateLog($"{addressLog}/details", result.Message.Notification.AspNetException, NotificationType.Error, HttpContext.User?.Identity?.Name);
                    await _publishEndpoint.Publish(message);
                    return Ok(result.Message.Notification);
                }

                return Ok(new NotificationViewModel<TViewModel>(result.Message.Model));
            }
            catch (Exception e)
            {
                var message = new CreateLog($"{addressLog}/details", e.Message, NotificationType.Error, HttpContext.User?.Identity?.Name);
                await _publishEndpoint.Publish(message);
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult<NotificationViewModel>> Create(TViewModel request)
        {
            if (!ModelState.IsValid)
                return Ok(new NotificationViewModel<TViewModel>(new[] { TypeOfErrors.DataNotValid }, request));

            try
            {
                var result = await _create.GetResponse<NotificationViewModel>(new TCreateRequest() { ModelRequest = request });
                if (result.Message.Type != NotificationType.Success)
                {
                    var message = new CreateLog($"{addressLog}/create", result.Message.AspNetException, NotificationType.Error, HttpContext.User?.Identity?.Name);
                    await _publishEndpoint.Publish(message);
                }
                return Ok(result.Message);
            }
            catch (Exception e)
            {
                var message = new CreateLog($"{addressLog}/create", e.Message, NotificationType.Error, HttpContext.User?.Identity?.Name);
                await _publishEndpoint.Publish(message);
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpPut]
        [Route("update")]
        public async Task<ActionResult<NotificationViewModel>> Update(TViewModel request)
        {
            if (!ModelState.IsValid)
                return Ok(new NotificationViewModel<TViewModel>(new[] { TypeOfErrors.DataNotValid }, request));


            try
            {
                var result = await _update.GetResponse<NotificationViewModel>(new TUpdateRequest() { ModelRequest = request });
                if (result.Message.Type != NotificationType.Success)
                {
                    var message = new CreateLog($"{addressLog}/update", result.Message.AspNetException, NotificationType.Error, HttpContext.User?.Identity?.Name);
                    await _publishEndpoint.Publish(message);
                }
                return Ok(result.Message);
            }
            catch (Exception e)
            {
                var message = new CreateLog($"{addressLog}/update", e.Message, NotificationType.Error, HttpContext.User?.Identity?.Name);
                await _publishEndpoint.Publish(message);
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<NotificationViewModel>> Delete(int id)
        {
            if (id == 0)
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.BadRequest }));

            try
            {
                var result = await _delete.GetResponse<NotificationViewModel>(new TDeleteRequest { ModelRequest = id });
                if (result.Message.Type != NotificationType.Success)
                {
                    var message = new CreateLog($"{addressLog}/delete", result.Message.AspNetException, NotificationType.Error, HttpContext.User?.Identity?.Name);
                    await _publishEndpoint.Publish(message);
                }
                return Ok(result.Message);
            }
            catch (Exception e)
            {
                var message = new CreateLog($"{addressLog}/delete", e.Message, NotificationType.Error, HttpContext.User?.Identity?.Name);
                await _publishEndpoint.Publish(message);
                return Ok(new NotificationViewModel(new[] { TypeOfErrors.InternalServerError }));
            }
        }
    }
}
