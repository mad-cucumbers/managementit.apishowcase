﻿using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Requests.ProductSpecificationRequests
{
    public class UpdateProductSpecificationRequest : UpdateEntityRequest<ProductSpecificationViewModel>
    {
    }
}
