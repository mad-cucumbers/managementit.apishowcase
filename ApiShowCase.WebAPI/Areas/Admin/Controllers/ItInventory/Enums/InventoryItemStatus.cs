﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Enums
{
    public enum InventoryItemStatus
    {
        OnStock = 0,                //На складе
        Issued = 1,                 // Выдана = в работе
        InRepair = 2,               // В ремонте
        Scrapped = 3,               // Списана
        WaitingToBeUtilised = 4,    // В ожидании утилизации
        Utilised = 5                // Утилизирована
    }
}
