﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class ProductSpecificationViewModel : BaseEntity
    {
        public string SpecificationId { get; set; }
        public string ProductSpecificationBlankId { get; set; }
    }
}
