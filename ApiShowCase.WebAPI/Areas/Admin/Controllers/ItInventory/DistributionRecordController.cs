﻿using InventoryIT.WebHost.Contracts.Requests.DistributionRecordRequests;
using InventoryIT.WebHost.Contracts.Responses.DistributionRecordResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class DistributionRecordController : BaseController<DistributionRecordViewModel,
                                                DistributionRecordViewModel,
                                                GetByIdDistributionRecordRequest,
                                                CreateDistributionRecordRequest,
                                                UpdateDistributionRecordRequest,
                                                DeleteDistributionRecordRequest,
                                                GetAllDistributionRecordResponse,
                                                GetByIdDistributionRecordResponse>
    {
        public DistributionRecordController(
            IRequestClient<DistributionRecordViewModel> getAll,
            IRequestClient<GetByIdDistributionRecordRequest> getById,
            IRequestClient<CreateDistributionRecordRequest> create,
            IRequestClient<UpdateDistributionRecordRequest> update,
            IRequestClient<DeleteDistributionRecordRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "DistributionRecord";
        }
    }
}
