﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class ProductSpecificationBlank : BaseEntity
    {
        public string ProductSpecificationBlankId { get; set; }
        public string SpecificationBlankId { get; set; }
        public virtual SpecificationBlank SpecificationBlank { get; set; }
        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual List<ProductSpecification> ProductSpecifications { get; set; }
    }
}
