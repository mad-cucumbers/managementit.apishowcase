﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    //Характиристика
    public class Specification : BaseEntity
    {
        public string SpecificationId { get; set; }//уникальное
        public bool IsRequered { get; set; }
        public string SpecificationBlankId { get; set; }
        public virtual SpecificationBlank SpecificationBlank { get; set; }
    }
}
