﻿using ApiShowCase.WebAPI.AddressConstatnts;
using Contracts.Enums;
using Contracts.Logs;
using Contracts.ResponseModels;
using InventoryIT.WebHost.Contracts.Requests.AmortisationReasonRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationReasonResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class AmortisationReasonController : BaseController<AmortisationReasonViewModel,
                                                AmortisationReasonViewModel,
                                                GetByIdAmortisationReasonRequest,
                                                CreateAmortisationReasonRequest,
                                                UpdateAmortisationReasonRequest,
                                                DeleteAmortisationReasonRequest,
                                                GetAllAmortisationReasonResponse,
                                                GetByIdAmortisationReasonResponse>
    {
        public AmortisationReasonController(
            IRequestClient<AmortisationReasonViewModel> getAll,
            IRequestClient<GetByIdAmortisationReasonRequest> getById,
            IRequestClient<CreateAmortisationReasonRequest> create,
            IRequestClient<UpdateAmortisationReasonRequest> update,
            IRequestClient<DeleteAmortisationReasonRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create,update,delete, publishEndpoint)
        {
            addressLog = "AmortisationReason";
        }

    }
}
