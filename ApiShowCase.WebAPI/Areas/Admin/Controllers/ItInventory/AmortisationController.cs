﻿using InventoryIT.WebHost.Contracts.Requests.AmortisationRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class AmortisationController : BaseController<AmortisationViewModel,
                                                AmortisationViewModel,
                                                GetByIdAmortisationRequest,
                                                CreateAmortisationRequest,
                                                UpdateAmortisationRequest,
                                                DeleteAmortisationRequest,
                                                GetAllAmortisationResponse,
                                                GetByIdAmortisationResponse>
    {
        public AmortisationController(
            IRequestClient<AmortisationViewModel> getAll,
            IRequestClient<GetByIdAmortisationRequest> getById,
            IRequestClient<CreateAmortisationRequest> create,
            IRequestClient<UpdateAmortisationRequest> update,
            IRequestClient<DeleteAmortisationRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "Amortisation";
        }
    }
}
