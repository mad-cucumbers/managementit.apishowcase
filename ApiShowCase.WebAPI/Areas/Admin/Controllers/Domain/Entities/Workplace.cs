﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InventoryIT.Core.Domain.Entities
{
    /// <summary>
    /// Рабочие место состоит из:
    /// Помещение где находится
    /// Отделение к которому принадлежит(может быть случай что в помещение одного отделения сидит сотрудник из другого)
    /// Компьютер
    /// Монитор(может быть несколько)
    /// Орг.техника(не обязательно)
    /// ИБП(не обязательно) (edited)
    /// </summary>
    public class Workplace : BaseEntity
    {
        public int WorkplaceId { get; set; }
        public int BuildingId { get; set; }
        public int RoomId { get; set; }
        public int DepartmentId { get; set; }
        public virtual List<DistributionRecord> DistributionRecords { get; set; }
        public virtual List<InventoryItem> InventoryItems { get; set; }
    }
}
