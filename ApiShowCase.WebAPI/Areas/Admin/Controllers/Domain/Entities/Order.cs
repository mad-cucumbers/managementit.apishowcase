﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class Order : BaseEntity
    {
        public string OrderId { get; set; }
        public virtual InventoryItemTransaction InventoryItemTransaction { get; set; }
        public DateTime OrderDate { get; set; }
        public string Comment { get; set; }
        public string DepartmentId { get; set; }
        public int EmployeeId { get; set; }
        public virtual List<OrderRecord> OrderRecords { get; set; }
    }
}
