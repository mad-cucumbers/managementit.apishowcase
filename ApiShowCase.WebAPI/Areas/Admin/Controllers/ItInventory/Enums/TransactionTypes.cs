﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Enums
{
    public enum TransactionType
    {
        None = 0,//пусто
        ReceiptOnWarehouse = 1,//Поступило на склад
        IssuedToEmployee = 2,//Выдано со склада
        ReturnedToWarehouse = 3,//Возвращено на склад
        WriteOff = 4
    }
}
