﻿using InventoryIT.Core.Domain;
using System.Collections.Generic;

namespace InventoryIT.WebHost.Models
{
    public class SpecificationBlankViewModel : BaseEntity
    {
        public string SpecificationBlankId { get; set; }
        public List<SpecificationViewModel> Specifications { get; set; }
    }
}
