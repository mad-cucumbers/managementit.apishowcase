﻿using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Requests.ProductSpecificationBlankRequests
{
    public class UpdateProductSpecificationBlankRequest : UpdateEntityRequest<ProductSpecificationBlankViewModel>
    {
    }
}
