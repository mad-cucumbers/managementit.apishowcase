﻿using InventoryIT.WebHost.Contracts.Requests.DistributionRequests;
using InventoryIT.WebHost.Contracts.Responses.DistributionResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class DistributionController : BaseController<DistributionViewModel,
                                                DistributionViewModel,
                                                GetByIdDistributionRequest,
                                                CreateDistributionRequest,
                                                UpdateDistributionRequest,
                                                DeleteDistributionRequest,
                                                GetAllDistributionResponse,
                                                GetByIdDistributionResponse>
    {
        public DistributionController(
            IRequestClient<DistributionViewModel> getAll,
            IRequestClient<GetByIdDistributionRequest> getById,
            IRequestClient<CreateDistributionRequest> create,
            IRequestClient<UpdateDistributionRequest> update,
            IRequestClient<DeleteDistributionRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "Distribution";
        }
    }
}
