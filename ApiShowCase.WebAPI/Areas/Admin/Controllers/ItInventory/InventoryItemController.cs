﻿using InventoryIT.WebHost.Contracts.Requests.InventoryItemRequests;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class InventoryItemController : BaseController<InventoryItemViewModel,
                                                InventoryItemViewModel,
                                                GetByIdInventoryItemRequest,
                                                CreateInventoryItemRequest,
                                                UpdateInventoryItemRequest,
                                                DeleteInventoryItemRequest,
                                                GetAllInventoryItemResponse,
                                                GetByIdInventoryItemResponse>
    {
        public InventoryItemController(
            IRequestClient<InventoryItemViewModel> getAll,
            IRequestClient<GetByIdInventoryItemRequest> getById,
            IRequestClient<CreateInventoryItemRequest> create,
            IRequestClient<UpdateInventoryItemRequest> update,
            IRequestClient<DeleteInventoryItemRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "InventoryItem";
        }
    }
}
