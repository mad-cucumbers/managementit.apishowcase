﻿using InventoryIT.WebHost.Contracts.Requests.OrderRequests;
using InventoryIT.WebHost.Contracts.Responses.OrderResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class OrderController : BaseController<OrderViewModel,
                                                OrderViewModel,
                                                GetByIdOrderRequest,
                                                CreateOrderRequest,
                                                UpdateOrderRequest,
                                                DeleteOrderRequest,
                                                GetAllOrderResponse,
                                                GetByIdOrderResponse>
    {
        public OrderController(
            IRequestClient<OrderViewModel> getAll,
            IRequestClient<GetByIdOrderRequest> getById,
            IRequestClient<CreateOrderRequest> create,
            IRequestClient<UpdateOrderRequest> update,
            IRequestClient<DeleteOrderRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "Order";
        }
    }
}
