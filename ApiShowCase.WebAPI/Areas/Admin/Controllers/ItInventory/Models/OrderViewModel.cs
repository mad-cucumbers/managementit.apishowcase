﻿using InventoryIT.Core.Domain;
using System;
using System.Collections.Generic;

namespace InventoryIT.WebHost.Models
{
    public class OrderViewModel : BaseEntity
    {
        public string OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string Comment { get; set; }
        public int ReceiptDepartmentId { get; set; }
        public int EmployeeId { get; set; }
        public List<OrderRecordViewModel> ReceiptOrderRecords { get; set; }
    }
}
