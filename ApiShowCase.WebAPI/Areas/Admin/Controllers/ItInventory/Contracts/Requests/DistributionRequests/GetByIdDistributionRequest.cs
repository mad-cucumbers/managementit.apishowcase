﻿using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Requests.DistributionRequests
{
    public class GetByIdDistributionRequest : GetByIdEntityRequest<int>
    {
    }
}
