﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class AmortisationReason : BaseEntity
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public virtual List<AmortisationRecord> AmortisationRecords { get; set; }
    }
}
