﻿using InventoryIT.WebHost.Contracts.Requests.WorkPlaceRequests;
using InventoryIT.WebHost.Contracts.Responses.WorkPlaceResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class WorkPlaceController : BaseController<WorkplaceViewModel,
                                                WorkplaceViewModel,
                                                GetByIdWorkPlaceRequest,
                                                CreateWorkPlaceRequest,
                                                UpdateWorkPlaceRequest,
                                                DeleteWorkPlaceRequest,
                                                GetAllWorkPlaceResponse,
                                                GetByIdWorkPlaceResponse>
    {
        public WorkPlaceController(
            IRequestClient<WorkplaceViewModel> getAll,
            IRequestClient<GetByIdWorkPlaceRequest> getById,
            IRequestClient<CreateWorkPlaceRequest> create,
            IRequestClient<UpdateWorkPlaceRequest> update,
            IRequestClient<DeleteWorkPlaceRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "WorkPlace";
        }
    }
}
