﻿using ApplicationContracts.ViewModels.Cashe;
using Contracts.ResponseModels;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.Cashe
{
    [Area("Admin")]
    [Route("[controller]")]
    public class MemoryCasheController : Controller
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public MemoryCasheController(IPublishEndpoint publishEndpoint) => 
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        
        [HttpGet]
        [Route("CasheClear")]
        public async Task<ActionResult<NotificationViewModel>> ApplicationCasheClear()
        {
            var request = new ClearMemoryCasheRequest();
            await _publishEndpoint.Publish(request);
            return Ok(new NotificationViewModel());
        }
    }
}
