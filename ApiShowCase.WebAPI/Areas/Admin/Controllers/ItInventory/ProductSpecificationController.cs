﻿using InventoryIT.WebHost.Contracts.Requests.ProductSpecificationRequests;
using InventoryIT.WebHost.Contracts.Responses.ProductSpecificationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory
{
    [Area("Admin")]
    [Route("[controller]")]
    public class ProductSpecificationController : BaseController<ProductSpecificationViewModel,
                                                ProductSpecificationViewModel,
                                                GetByIdProductSpecificationRequest,
                                                CreateProductSpecificationRequest,
                                                UpdateProductSpecificationRequest,
                                                DeleteProductSpecificationRequest,
                                                GetAllProductSpecificationResponse,
                                                GetByIdProductSpecificationResponse>
    {
        public ProductSpecificationController(
            IRequestClient<ProductSpecificationViewModel> getAll,
            IRequestClient<GetByIdProductSpecificationRequest> getById,
            IRequestClient<CreateProductSpecificationRequest> create,
            IRequestClient<UpdateProductSpecificationRequest> update,
            IRequestClient<DeleteProductSpecificationRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "ProductSpecification";
        }
    }
}
