﻿using ApiShowCase.WebAPI.AddressConstatnts;
using ApiShowCase.WebAPI.Areas.Admin.Controllers.ItInventory;
using Contracts.Enums;
using Contracts.Logs;
using Contracts.ResponseModels;
using InventoryIT.WebHost.Contracts.Requests.SpecificationBlankRequests;
using InventoryIT.WebHost.Contracts.Responses.SpecificationBlankResponse;
using InventoryIT.WebHost.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.SpecificationResponse
{
    [Area("Admin")]
    [Route("[controller]")]
    public class SpecificationBlankController : BaseController<SpecificationBlankViewModel,
                                                SpecificationBlankViewModel,
                                                GetByIdSpecificationBlankRequest,
                                                CreateSpecificationBlankRequest,
                                                UpdateSpecificationBlankRequest,
                                                DeleteSpecificationBlankRequest,
                                                GetAllSpecificationBlankResponse,
                                                GetByIdSpecificationBlankResponse>
    {
        public SpecificationBlankController(
            IRequestClient<SpecificationBlankViewModel> getAll,
            IRequestClient<GetByIdSpecificationBlankRequest> getById,
            IRequestClient<CreateSpecificationBlankRequest> create,
            IRequestClient<UpdateSpecificationBlankRequest> update,
            IRequestClient<DeleteSpecificationBlankRequest> delete,
            IPublishEndpoint publishEndpoint) : base(getAll, getById, create, update, delete, publishEndpoint)
        {
            addressLog = "SpecificationBlank";
        }
    }
}
