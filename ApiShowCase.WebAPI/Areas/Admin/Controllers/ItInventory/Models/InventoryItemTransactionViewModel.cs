﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Enums;
using System;

namespace InventoryIT.WebHost.Models
{
    public class InventoryItemTransactionViewModel : BaseEntity
    {
        public DateTime OperationDate { get; set; }
        public string InventoryId { get; set; }
        public string DocNum { get; set; }
        public TransactionType TransactionType { get; set; }
        public float Quantity { get; set; }
    }
}
